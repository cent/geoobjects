# -*- encoding: utf-8 -*-
import struct
import socket
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.core.exceptions import FieldError
from django.utils.functional import cached_property


__author__ = 'vadim'

DELIM = ':'

DEFAULT_GEO_LOCATION = 5279

CACHE_TIMEOUT = 86400

class GeobazaRanges(models.Model):
    start = models.CharField(max_length=15)
    end = models.CharField(max_length=15)
    ip_int = models.BigIntegerField(db_index=True)
    length = models.BigIntegerField(db_index=True)
    tld = models.CharField(max_length=4, null=True)
    obj_id = models.PositiveIntegerField(max_length=11)

    class Meta:
        unique_together = ('ip_int', 'length')

    def __unicode__(self):
        return u"%s-%s" % (self.start, self.end)

    @classmethod
    def get_obj(cls, ip):
        int_ip = struct.unpack('>L', socket.inet_aton(ip))[0]
        key_cache = "geo_range_" + str(ip)
        ob = cache.get(key_cache)
        if ob is None:
            try:
                ob = cls.objects.all().extra(where=['ip_int <= %d AND ip_int + length >=%d' % (int_ip, int_ip)],
                                             order_by=['length']).get()
                cache.set(key_cache, ob, CACHE_TIMEOUT)
            except ObjectDoesNotExist:
                ob = None
        return ob

    @cached_property
    def obj(self):
        ob = GeobazaObjects.get_obj(self.obj_id)
        if not ob:
            ob = GeobazaObjects.get_obj(DEFAULT_GEO_LOCATION)
        return ob


class GeobazaObjects(models.Model):
    parent_id = models.PositiveIntegerField()
    type = models.CharField(max_length=100, db_index=True)
    name_en = models.CharField(max_length=255, db_index=True)
    name_ru = models.CharField(max_length=255, null=True, default=None, db_index=True)
    lat = models.FloatField(null=True, default=None)
    lon = models.FloatField(null=True, default=None)

    def __unicode__(self):
        return u"%s:%s" % (self.type, self.name_en)

    @classmethod
    def get_obj(cls, obj_id, ):
        if obj_id == 0 or obj_id == '0':
            return None
        key_cache = "geo_obj_" + str(obj_id)
        ob = cache.get(key_cache)
        if ob is None:
            try:
                ob = GeobazaObjects.objects.get(pk=obj_id)
            except ObjectDoesNotExist:
                ob = None
            cache.set(key_cache, ob, CACHE_TIMEOUT)
        return ob

    @cached_property
    def parent(self):
        return GeobazaObjects.get_obj(self.parent_id)

    @cached_property
    def child(self):
        key_cache = "geo_obj_child_%d" % self.id
        objs = cache.get(key_cache)
        if objs is None:
            objs = list(GeobazaObjects.objects.all().filter(parent_id=self.id))
            cache.set(key_cache, objs, CACHE_TIMEOUT)
        return objs

    def get_path(self, reverse=False):
        path = [self]
        parent = self.parent
        while parent:
            path.append(parent)
            parent = parent.parent
        if reverse:
            path.reverse()
        return path

    def to_dict(self, lang='ru'):
        return {self.id: self.name(lang)}

    @cached_property
    def path(self):
        return self.get_path()

    @cached_property
    def country(self):
        for obj in self.path:
            if obj.type == 'country':
                return obj
        return GeobazaObjects(type='country', name_en='', name_ru='')

    def name(self, lang='en', skip=['region']):
        return u', '.join([path.name_ru if lang == 'ru' and path.name_ru else path.name_en
                           for path in self.get_path(False) if not path.type in skip])

    @cached_property
    def dump_path(self):
        path = self.path
        if not path:  # or (hasattr(path, 'is_special') and path.is_special):
            return ''
        ret = DELIM.join([str(obj.id) for obj in path])
        return DELIM + ret + DELIM

    @cached_property
    def dump_key(self):
        return DELIM + str(self.id) + DELIM


def reduce_int(type_value, value, default=None):
    try:
        return type_value(value)
    except:
        return default


class GeoField(models.CharField):
    __metaclass__ = models.SubfieldBase
    description = "Stores GEO data"

    def __init__(self, *args, **kwargs):
        self.type_field = kwargs.get('type', list)  # list (default) or dict
        super(GeoField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        if value is None:
            return None
        if isinstance(value, (list, tuple)) and len(value) > 1:
            ob = GeobazaObjects.get_obj(value[0])
            value = ob.dump_path if ob else None
        elif isinstance(value, dict) and len(value):
            ob = GeobazaObjects.get_obj(value.keys()[0])
            value = ob.dump_path if ob else None
        elif isinstance(value, int):
            ob = GeobazaObjects.get_obj(value)
            value = ob.dump_path if ob else None
        elif isinstance(value, basestring) and (DELIM in value or not value):
            pass
        else:
            raise FieldError('GeoField take value type=%s. Only dict, int, path support!' % type(value))

        return value

    def to_python(self, value):
        if isinstance(value, basestring) and len(value):
            value_split = value.split(DELIM) if DELIM in value else [value]
            value_split = [reduce_int(int, v) for v in value_split if v]
            value = value_split[0] if len(value_split) > 0 else None
            if value:
                ob = GeobazaObjects.get_obj(value)
                if ob:
                    value = ob.to_dict()
                    if self.type_field == list:
                        value = value.items()[0]
        return value

    def south_field_triple(self):
        """Returns a suitable description of this field for South."""
        # We'll just introspect the _actual_ field.
        try:
            from south.modelsinspector import introspector
            field_class = self.__class__.__module__ + "." + self.__class__.__name__
            args, kwargs = introspector(self)
            # That's our definition!
            return field_class, args, kwargs
        except:
            pass

