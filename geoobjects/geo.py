# -*- encoding: utf-8 -*-
from django.db.models import Q
from django.core.cache import cache
from models import GeobazaObjects, GeobazaRanges, CACHE_TIMEOUT


__author__ = 'vadim'


class GeoObject(object):
    def __init__(self, request=None, ip=None):
        self.ip = ip or request.META['REMOTE_ADDR']

    @property
    def range(self):
        try:
            return GeobazaRanges.get_obj(self.ip)
        except:
            pass

    @property
    def obj(self):
        try:
            return self.range.obj
        except:
            pass

    @property
    def path(self):
        try:
            return self.obj.path
        except:
            pass

    @property
    def dump_path(self):
        try:
            return self.obj.dump_path
        except:
            pass

    @property
    def dump_key(self):
        try:
            return self.obj.dump_key
        except:
            pass


def get_object_start(start, location=None, limit=0):
    where_args = [Q(name_en__istartswith=start) | Q(name_ru__istartswith=start)]

    if location and isinstance(location, (list, tuple)) and len(location):
        if 'country' in location and 'region' in location and 'locality' in location:
            where_args.append(Q(type='country') | Q(type='region') | Q(type='locality'))
        elif 'country' in location and 'region' in location:
            where_args.append(Q(type='country') | Q(type='region'))
        elif 'country' in location and 'locality' in location:
            where_args.append(Q(type='country') | Q(type='locality'))
        elif 'region' in location and 'locality' in location:
            where_args.append(Q(type='region') | Q(type='locality'))
        elif 'country' in location:
            where_args.append(Q(type='country'))
        elif 'region' in location:
            where_args.append(Q(type='region'))
        elif 'locality' in location:
            where_args.append(Q(type='locality'))

    key_cache = "geo_st_%s_%s_%s" % (start, ''.join(location) if location else '', limit)
    objs = cache.get(key_cache)
    if not objs:
        query = GeobazaObjects.objects.all()
        objs = list(query.filter(*where_args)[:limit]) if limit > 0 else list(query.filter(*where_args))
        cache.set(key_cache, objs, CACHE_TIMEOUT)
    return objs