# -*- encoding: utf-8 -*-
from django.contrib import admin
from geoobjects.models import GeobazaObjects, GeobazaRanges

__author__ = 'vadim'


class GeobazaRangesAdmin(admin.ModelAdmin):
    list_display = ('start', 'end', 'length', 'tld', 'obj_id')
admin.site.register(GeobazaRanges, GeobazaRangesAdmin)


class GeobazaObjectsAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'type', 'parent_id', 'lat', 'lon')
admin.site.register(GeobazaObjects, GeobazaObjectsAdmin)

