# -*- encoding: utf-8 -*-
from django.test import TestCase
from geoobjects.geo import GeoObject, get_object_start
from geoobjects.models import GeobazaObjects


def print_objs(objs):
    for obj in objs:
        print u"%d -> %d [%s]: en:%s - ru:%s geo(%f, %f)" % (obj.id,
                                                             obj.parent_id,
                                                             obj.type,
                                                             obj.name_en,
                                                             obj.name_ru if obj.name_ru else u"Нет",
                                                             obj.lat if obj.lat else 0.0,
                                                             obj.lon if obj.lon else 0.0)
    print u"Count objects = %d" % len(objs)


def print_obj(obj, attributes):
    print "-" * 30
    for attr in attributes:
        if hasattr(obj, attr):
            print "%s = %s" % (attr, getattr(obj, attr))


class GeobazaQueryTest(TestCase):
    fixtures = ['test_geobazaranges.json', 'test_geobazaobjects.json']

    def setUp(self):
        self.vn = GeoObject(ip='80.250.189.254')
        self.cf = GeoObject(ip='8.8.8.8')

    def test_instance(self):
        self.assertIsInstance(self.vn, GeoObject)
        self.assertIsInstance(self.cf, GeoObject)

    def test_point(self):
        point = self.vn.obj
        self.assertEqual(point.id, 5069)
        # self.assertEqual(point.iso_id, None)
        self.assertEqual(point.type, 'locality')
        self.assertEqual(point.name_en, 'Velikiy Novgorod')
        # self.assertEqual(point.level, 3)
#        from pprint import pprint
#        pprint(point.translations)

    def test_path(self):
        path = self.vn.path
        for obj in path:
            self.assertIsInstance(obj.id, int)
            self.assertIsInstance(obj.type, basestring)
            # self.assertIsInstance(obj.name, basestring)
            # self.assertIsInstance(obj.level, int)
#            self.print_obj(obj, ['id', 'iso_id', 'type', 'name', 'level'])

    def test_dump_path(self):
        path = self.vn.dump_path
        self.assertEqual(path, ':69:1271:4710:5069:')


class GeobazaRangesTest(TestCase):
    fixtures = ['test_geobazaranges.json', 'test_geobazaobjects.json']

    def setUp(self):
        self.vn = GeoObject(ip='80.250.189.254')
        self.cf = GeoObject(ip='8.8.8.8')

    def test_instance(self):
        self.assertIsInstance(self.vn, GeoObject)
        self.assertIsInstance(self.cf, GeoObject)

    def x_test_point(self):
        point = self.vn.get
        self.assertTrue(point)
        self.assertEqual(point.id, 5069)
        self.assertEqual(point.iso_id, None)
        self.assertEqual(point.type, 'locality')
        self.assertEqual(point.name, 'Velikiy Novgorod')
        self.assertEqual(point.level, 3)
    #        from pprint import pprint
    #        pprint(point.translations)

    def test_path(self):
        path = self.vn.path
        for obj in path:
            self.assertIsInstance(obj.id, int)
            self.assertIsInstance(obj.type, basestring)
            self.assertIsInstance(obj.name_en, basestring)
        #            self.print_obj(obj, ['id', 'iso_id', 'type', 'name', 'level'])

    def test_dump_path(self):
        self.assertEqual(self.vn.dump_path, ':69:1271:4710:5069:')


class GeobazaObjectsTest(TestCase):

    fixtures = ['test_geobazaobjects.json',]

    def test_get_object_start(self):
        objs = get_object_start('XXXXXXXXXX')
        self.assertEqual(len(objs), 0)

        objs = get_object_start('Samoa')
        self.assertEqual(len(objs), 1)

        objs = get_object_start('Hon')
        self.assertEqual(len(objs), 2)

        objs = get_object_start('Вел')  # Великий Новгород
        # print_objs(objs)
        self.assertEqual(len(objs), 3)

        objs = get_object_start('Вел', location=['country'])  # Великобритания
        self.assertEqual(len(objs), 1)
        self.assertEqual(objs[0].name_en, 'United Kingdom')

        objs = get_object_start('Вел', location=['region'])  # Велико-тырновская область
        self.assertEqual(len(objs), 1)
        self.assertEqual(objs[0].name_en, 'Veliko Tarnovo')

        objs = get_object_start('Вел', location=['locality'])  # Великий Новгород
        self.assertEqual(len(objs), 1)
        self.assertEqual(objs[0].name_en, 'Velikiy Novgorod')
        self.assertEqual(objs[0].parent.id, 4710)
        self.assertEqual(len(objs[0].parent.child), 5)

#        objs = GeobazaObjects.objects.all()
#        self.assertGreater(len(objs), 0)

    def test_path(self):
        objs = get_object_start('Вел', location=['locality'])  # Великий Новгород
        self.assertEqual(len(objs[0].path), 4)

    def test_name(self):
        objs = get_object_start('Вел', location=['locality'])  # Великий Новгород
        self.assertEqual(objs[0].name(), u'Velikiy Novgorod')
        self.assertEqual(objs[0].name('ru'), u'Великий Новгород')

    def test_dump_key(self):
        objs = get_object_start('Вел', location=['locality'])  # Великий Новгород
        self.assertEqual(objs[0].dump_key, ':5069:')

    def test_dump_path(self):
        objs = get_object_start('Вел', location=['locality'])  # Великий Новгород
        self.assertEqual(objs[0].dump_path, ':69:1271:4710:5069:')
