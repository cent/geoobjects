# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GeobazaRanges'
        db.create_table(u'geoobjects_geobazaranges', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('end', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('ip_int', self.gf('django.db.models.fields.BigIntegerField')()),
            ('length', self.gf('django.db.models.fields.BigIntegerField')()),
            ('tld', self.gf('django.db.models.fields.CharField')(max_length=4, null=True)),
            ('obj_id', self.gf('django.db.models.fields.PositiveIntegerField')(max_length=11)),
        ))
        db.send_create_signal(u'geoobjects', ['GeobazaRanges'])

        # Adding unique constraint on 'GeobazaRanges', fields ['ip_int', 'length']
        db.create_unique(u'geoobjects_geobazaranges', ['ip_int', 'length'])

        # Adding model 'GeobazaObjects'
        db.create_table(u'geoobjects_geobazaobjects', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True)),
            ('lat', self.gf('django.db.models.fields.FloatField')(default=None, null=True)),
            ('lon', self.gf('django.db.models.fields.FloatField')(default=None, null=True)),
        ))
        db.send_create_signal(u'geoobjects', ['GeobazaObjects'])


    def backwards(self, orm):
        # Removing unique constraint on 'GeobazaRanges', fields ['ip_int', 'length']
        db.delete_unique(u'geoobjects_geobazaranges', ['ip_int', 'length'])

        # Deleting model 'GeobazaRanges'
        db.delete_table(u'geoobjects_geobazaranges')

        # Deleting model 'GeobazaObjects'
        db.delete_table(u'geoobjects_geobazaobjects')


    models = {
        u'geoobjects.geobazaobjects': {
            'Meta': {'object_name': 'GeobazaObjects'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True'}),
            'lon': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True'}),
            'parent_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'geoobjects.geobazaranges': {
            'Meta': {'unique_together': "(('ip_int', 'length'),)", 'object_name': 'GeobazaRanges'},
            'end': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_int': ('django.db.models.fields.BigIntegerField', [], {}),
            'length': ('django.db.models.fields.BigIntegerField', [], {}),
            'obj_id': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '11'}),
            'start': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tld': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'})
        }
    }

    complete_apps = ['geoobjects']