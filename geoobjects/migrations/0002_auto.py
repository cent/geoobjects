# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'GeobazaObjects', fields ['name_ru']
        db.create_index(u'geoobjects_geobazaobjects', ['name_ru'])

        # Adding index on 'GeobazaObjects', fields ['name_en']
        db.create_index(u'geoobjects_geobazaobjects', ['name_en'])

        # Adding index on 'GeobazaObjects', fields ['type']
        db.create_index(u'geoobjects_geobazaobjects', ['type'])

        # Adding index on 'GeobazaRanges', fields ['ip_int']
        db.create_index(u'geoobjects_geobazaranges', ['ip_int'])

        # Adding index on 'GeobazaRanges', fields ['length']
        db.create_index(u'geoobjects_geobazaranges', ['length'])


    def backwards(self, orm):
        # Removing index on 'GeobazaRanges', fields ['length']
        db.delete_index(u'geoobjects_geobazaranges', ['length'])

        # Removing index on 'GeobazaRanges', fields ['ip_int']
        db.delete_index(u'geoobjects_geobazaranges', ['ip_int'])

        # Removing index on 'GeobazaObjects', fields ['type']
        db.delete_index(u'geoobjects_geobazaobjects', ['type'])

        # Removing index on 'GeobazaObjects', fields ['name_en']
        db.delete_index(u'geoobjects_geobazaobjects', ['name_en'])

        # Removing index on 'GeobazaObjects', fields ['name_ru']
        db.delete_index(u'geoobjects_geobazaobjects', ['name_ru'])


    models = {
        u'geoobjects.geobazaobjects': {
            'Meta': {'object_name': 'GeobazaObjects'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True'}),
            'lon': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'db_index': 'True'}),
            'parent_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'})
        },
        u'geoobjects.geobazaranges': {
            'Meta': {'unique_together': "(('ip_int', 'length'),)", 'object_name': 'GeobazaRanges'},
            'end': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_int': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True'}),
            'length': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True'}),
            'obj_id': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '11'}),
            'start': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tld': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'})
        }
    }

    complete_apps = ['geoobjects']