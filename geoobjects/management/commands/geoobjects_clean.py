# -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand
from geoobjects import models

class Command(BaseCommand):
    """Import category trees from a file."""

    help = "Clean all data from geoobject table."
    args = "None"

    def handle(self, *file_paths, **options):
        """
        Handle the basic import
        """

        print "Clean ...",
        model = models.GeobazaObjects.objects.all()
        model.delete()
        print "Done"
