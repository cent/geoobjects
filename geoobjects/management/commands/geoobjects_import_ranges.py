# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError
from geoobjects.models import GeobazaRanges
import os
import csv
from pprint import pprint

__author__ = 'vadim'


def convert(val, typ, default=None):
    try:
        return typ(val)
    except:
        return default


class Command(BaseCommand):
    help = 'Import Geobaza ranges'
    args = 'geobaza.ranges.csv'

    BULK_SIZE = 10000

    def handle(self, *file_paths, **options):

        skip_first = True

        start_id = int(file_paths[1]) if len(file_paths) > 1 else 1
        file_path = file_paths[0]

        if not os.path.isfile(file_path):
            print 'File %s not found!' % file_path
        lines = csv.reader(open(file_path, 'rb'))

        self.import_data_bulk(lines, start_id)

    def geo_obj(self, data):
        return GeobazaRanges(
                    start=data[0].strip('" '),
                    end=data[1].strip('" '),
                    ip_int=convert(data[2], int),
                    length=convert(data[3], int, 0),
                    tld=data[4].strip('" '),
                    obj_id=convert(data[5], int, 0),
                )

    def import_data(self, lines, start_id):
        err_list = []
        for num, data in enumerate(lines):
            if num < start_id:
                continue
#            print "%d %s" % (num, "_" * 40)
#            pprint(data)
            try:
                obj = self.geo_obj(data)
                obj.save()
            except Exception, e:
                err_list.append('Error import id=%s (%s)' % (data[0], e))
                print 'Error import id=%s' % data[0]
        pprint(err_list)
        print "Errors - %d" % len(err_list)

    def import_data_bulk(self, lines, start_id):
        def create_bulk(bulk, err_list):
            try:
                GeobazaRanges.objects.bulk_create(bulk)
            except Exception, e:
                err_list.append('Error import id=%s (%s)' % (data[0], e))

        err_list = []
        bulk = []
        for num, data in enumerate(lines):
            if num < start_id:
                continue
            bulk.append(self.geo_obj(data))
            if len(bulk) >= self.BULK_SIZE:
                print "%s - %s" % (num-self.BULK_SIZE, num)
                create_bulk(bulk, err_list)
                bulk = []

        if len(bulk) > 0:
            create_bulk(bulk, err_list)
        pprint(err_list)
        print "Errors - %d" % len(err_list)
