# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError
from geoobjects.models import GeobazaObjects
import os
import csv
from pprint import pprint

__author__ = 'vadim'


def convert(val, typ, default=None):
    try:
        return typ(val)
    except:
        return default


class Command(BaseCommand):
    help = 'Import Geobaza ranges'
    args = 'geobaza.ranges.csv'

    def handle(self, *file_paths, **options):

        skip_first = True

        for file_path in file_paths:
            if not os.path.isfile(file_path):
                print 'File %s not found!' % file_path
                continue
            lines = csv.reader(open(file_path, 'rb'))

            self.import_data_bulk(lines, skip_first)

    def geo_obj(self, data):
        return GeobazaObjects(
                    id=convert(data[0], int),
                    parent_id=convert(data[1], int, 0),
                    type=data[2].strip('" '),
                    name_en=data[3].strip('" '),
                    name_ru=data[4].strip('" '),
                    lat=convert(data[5], float),
                    lon=convert(data[6], float),
                )

    def import_data(self, lines, skip_first):
        err_list = []
        for num, data in enumerate(lines):
            if not num and skip_first:
                continue
#            print "%d %s" % (num, "_" * 40)
#            pprint(data)
            try:
                obj = self.geo_obj(data)
                obj.save()
            except IntegrityError, e:
                err_list.append('Error import id=%s (%s)' % (data[0], e))
                print 'Error import id=%s' % data[0]
        pprint(err_list)
        print "Errors - %d" % len(err_list)

    def import_data_bulk(self, lines, skip_first):
        def create_bulk(bulk, err_list):
            try:
                GeobazaObjects.objects.bulk_create(bulk)
            except IntegrityError, e:
                err_list.append('Error import id=%s (%s)' % (data[0], e))

        err_list = []
        bulk = []
        BULK_SIZE = 100
        for num, data in enumerate(lines):
            if not num and skip_first:
                continue
            bulk.append(self.geo_obj(data))
            if len(bulk) >= BULK_SIZE:
                create_bulk(bulk, err_list)
                bulk = []

        if len(bulk) > 0:
            create_bulk(bulk, err_list)
        pprint(err_list)
        print "Errors - %d" % len(err_list)
