# -*- encoding: utf-8 -*-
from geo import GeoObject

__author__ = 'vadim'


class GeoMiddleware(object):
    def process_request(self, request):
        request.geo = GeoObject(request)
