from distutils.core import setup

setup(
    name='geoobjects',
    version='0.2.6.1',
    packages=['geoobjects', 'geoobjects.management',
              'geoobjects.management.commands',
              'geoobjects.migrations'],
    scripts=['scripts/geoobjects_import_mysql.sh'],
    install_requires=['django'],
    url='https://cent@bitbucket.org/cent/geoobjects.git',
    license='GNU General Public License (GPL)',
    author='Vadim Statishin',
    author_email='statishin@gmail.com',
    description='Geo extension for django based on geobaza (http://geobaza.ru)'
)
