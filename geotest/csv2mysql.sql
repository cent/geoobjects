CREATE TABLE geoobjects_geobazaranges (
    start CHAR(15) NOT NULL,
    end CHAR(15) NOT NULL,
    ip_int INT UNSIGNED NOT NULL,
    length INT UNSIGNED NOT NULL,
    tld CHAR(4), id INT NOT NULL
);

LOAD DATA LOCAL INFILE '/tmp/geobaza.ranges.csv'
    INTO TABLE geoobjects_geobazaranges
    CHARACTER SET utf8
    FIELDS TERMINATED BY ', '
    ENCLOSED BY '"'
    (start, end, ip_int, length, tld, id)
    IGNORE 1 LINES;

CREATE TABLE geoobjects_geobazaobjects (
    id INT NOT NULL UNIQUE,
    parent_id INT NOT NULL,
    type VARCHAR(100),
    name_en VARCHAR(255),
    name_ru VARCHAR(255),
    lat FLOAT,
    lon FLOAT
) CHARSET utf8;

load data local infile '/tmp/geobaza.objects.csv'
    INTO TABLE geoobjects_geobazaobjects
    CHARACTER SET utf8
    FIELDS TERMINATED BY ', '
    ENCLOSED BY '"'
    (id, parent_id, type, name_en, name_ru, lat, lon)
    IGNORE 1 LINES;

