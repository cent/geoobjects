#!/bin/sh

DB=$1
IN1=$2
IN2=$3

GEOOBJ=/tmp/geobaza.objects.$$.csv
GEORNG=/tmp/geobaza.ranges.$$.csv


cat $IN1|grep -v "parent_id">${GEOOBJ}

echo "load data local infile '${GEOOBJ}' \
    INTO TABLE geoobjects_geobazaobjects \
    CHARACTER SET utf8 \
    FIELDS TERMINATED BY ', ' \
    ENCLOSED BY '\"' \
    (id, parent_id, type, name_en, name_ru, lat, lon);"| mysql --local-infile -u root -p ${DB}

rm ${GEOOBJ}

cat $IN2|grep -v "ip_start">${GEORNG}

echo "LOAD DATA LOCAL INFILE '${GEORNG}' \
    INTO TABLE geoobjects_geobazaranges \
    CHARACTER SET utf8 \
    FIELDS TERMINATED BY ', ' ENCLOSED BY '\"' \
    (start, end, ip_int, length, tld, obj_id);"|mysql --local-infile -u root -p ${DB}

rm ${GEORNG}
