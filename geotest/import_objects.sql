load data local infile 'geobaza.objects.csv'
    INTO TABLE geoobjects_geobazaobjects
    CHARACTER SET utf8
    FIELDS TERMINATED BY ', '
    ENCLOSED BY '"'
    (id, parent_id, type, name_en, name_ru, lat, lon);

