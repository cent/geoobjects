LOAD DATA LOCAL INFILE 'geobaza.ranges.csv'
    INTO TABLE geoobjects_geobazaranges
    CHARACTER SET utf8
    FIELDS TERMINATED BY ', '
    ENCLOSED BY '"'
    (start, end, ip_int, length, tld, obj_id);

